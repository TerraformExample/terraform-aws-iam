
data "aws_iam_policy_document" "superadmin" {
    statement {
        sid = "SuperAdminAccess"
        actions = ["*"]
        resources = ["*"]
    }
}

data "aws_iam_policy_document" "superadmin_assume_role" {
    statement {
        sid = "SuperAdminRoleAccess"
        actions = ["sts:AssumeRole"]
        principals {
            type = "AWS"
            identifiers = [
                "arn:aws:iam::${var.superadmin_access_account}:user/aws-assumerole"
            ]
        }
    }
}
