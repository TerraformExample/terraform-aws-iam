
resource "aws_iam_role" "superadmin" {
    name = "superadmin"
    assume_role_policy = data.aws_iam_policy_document.superadmin_assume_role.json
}
