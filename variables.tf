
variable "superadmin_access_account" {
    description = "The AWS account ID which is given access to the superadmin IAM Role"
}
